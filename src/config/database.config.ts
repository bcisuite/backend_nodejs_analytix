require('dotenv').config();

export default {
    oracle: {
        anzTnlm: {
            host: process.env['ANZ_TNLM_DB_HOST'],
            port: process.env['ANZ_TNLM_DB_PORT'],
            sid: process.env['ANZ_TNLM_DB_SID'],
            username: process.env['ANZ_TNLM_DB_USERNAME'],
            password: process.env['ANZ_TNLM_DB_PASSWORD'],
            poolSize: parseInt(process.env['ANZ_TNLM_DB_CONNECTION_POOL_SIZE'] || '4'),
        },
        asiaTnlm: {
            host: process.env['ASIA_TNLM_DB_HOST'],
            port: process.env['ASIA_TNLM_DB_PORT'],
            sid: process.env['ASIA_TNLM_DB_SID'],
            username: process.env['ASIA_TNLM_DB_USERNAME'],
            password: process.env['ASIA_TNLM_DB_PASSWORD'],
            poolSize: parseInt(process.env['ASIA_TNLM_DB_CONNECTION_POOL_SIZE'] || '4'),
        },
        schema: {
            mv: process.env['DASHBOARD_MV_SCHEMA'],
            common: process.env['DASHBOARD_COMMON_SCHEMA'],
            services: process.env['DASHBOARD_SERVICES_SCHEMA'],
        }
    },
};
