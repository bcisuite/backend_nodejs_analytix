import {ApplicationConfig, BackendNodejsAnalytixApplication} from './application';
import dbConfig from './config/database.config';
import oracledb from 'oracledb';

export * from './application';

export async function main(options: ApplicationConfig = {}) {
  // create database connection pool
  try {
    if (dbConfig.oracle.anzTnlm.host && dbConfig.oracle.anzTnlm.port && dbConfig.oracle.anzTnlm.sid &&
      dbConfig.oracle.anzTnlm.username && dbConfig.oracle.anzTnlm.password && dbConfig.oracle.anzTnlm.poolSize) {
      await oracledb.createPool({
        poolAlias: 'anzTnlm',
        connectString: `${dbConfig.oracle.anzTnlm.host}:${dbConfig.oracle.anzTnlm.port}/${dbConfig.oracle.anzTnlm.sid}`,
        user: dbConfig.oracle.anzTnlm.username,
        password: dbConfig.oracle.anzTnlm.password,
        stmtCacheSize: 16,
        poolMax: dbConfig.oracle.anzTnlm.poolSize,
        poolMin: dbConfig.oracle.anzTnlm.poolSize,
        poolIncrement: 0,
      });
    }

    if (dbConfig.oracle.asiaTnlm.host && dbConfig.oracle.asiaTnlm.port && dbConfig.oracle.asiaTnlm.sid &&
      dbConfig.oracle.asiaTnlm.username && dbConfig.oracle.asiaTnlm.password && dbConfig.oracle.asiaTnlm.poolSize) {
      await oracledb.createPool({
        poolAlias: 'asiaTnlm',
        connectString: `${dbConfig.oracle.asiaTnlm.host}:${dbConfig.oracle.asiaTnlm.port}/${dbConfig.oracle.asiaTnlm.sid}`,
        user: dbConfig.oracle.asiaTnlm.username,
        password: dbConfig.oracle.asiaTnlm.password,
        stmtCacheSize: 16,
        poolMax: dbConfig.oracle.asiaTnlm.poolSize,
        poolMin: dbConfig.oracle.asiaTnlm.poolSize,
        poolIncrement: 0,
      });
    }
  } catch (e) {
    throw e;
  }
  const app = new BackendNodejsAnalytixApplication(options);
  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}

async function closeConnectionPoolAndExit() {
  console.log("Terminating...");
  try {
    await oracledb.getPool('anzTnlm').close(15);
  } catch (err) {
    console.error('Failed to close connection pool anzTnlm due to: ', err);
  }
  try {
    await oracledb.getPool('asiaTnlm').close(15);
  } catch (err) {
    console.error('Failed to close connection pool asiaTnlm due to: ', err);
    process.exit(1);
  }
  process.exit(0);
}

if (require.main === module) {
  // Close the pool cleanly if Node.js is interrupted
  process
    .once('SIGTERM', closeConnectionPoolAndExit)
    .once('SIGINT', closeConnectionPoolAndExit);
  // Run the application
  const config = {
    rest: {
      port: +(process.env.PORT ?? 3000),
      host: process.env.HOST,
      // The `gracePeriodForClose` provides a graceful close for http/https
      // servers with keep-alive clients. The default value is `Infinity`
      // (don't force-close). If you want to immediately destroy all sockets
      // upon stop, set its value to `0`.
      // See https://www.npmjs.com/package/stoppable
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
    },
  };
  main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
